//
//  NDITest.hpp
//  NDITest
//
//  Created by Viacheslav Matchenko on 12/16/18.
//  Copyright © 2018 Viacheslav Matchenko. All rights reserved.
//

#include "Processing.NDI.Lib.h"

#ifndef NDITest_
#define NDITest_

/* The classes below are exported */
#pragma GCC visibility push(default)

void updateFrameBuffer(uint8_t* pData);

int ndiTestFunc(uint8_t* pData, uint8_t bytesPerLine);

#pragma GCC visibility pop
#endif
