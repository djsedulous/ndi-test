//
//  NDITest.cpp
//  Test1
//
//  Created by Viacheslav Matchenko on 12/15/18.
//  Copyright © 2018 Viacheslav Matchenko. All rights reserved.
//

#include <csignal>
#include <string>
#include <cstddef>
#include <cstring>
#include <cstdint>
#include <cstdio>
#include <atomic>
#include <chrono>
#include "NDITest.h"

static std::atomic<bool> exit_loop(false);
static void sigint_handler(int)
{
    exit_loop = true;
}

NDIlib_video_frame_v2_t NDI_video_frame;

void updateFrameBuffer(uint8_t* pData) {
    NDI_video_frame.p_data = pData;
}

int ndiTestFunc(uint8_t* pData, uint8_t bytesPerLine) {
    if (NDIlib_initialize()) {
        const NDIlib_v3* p_NDILib = NDIlib_v3_load();
        
        // We can now run as usual
        if (!p_NDILib->NDIlib_initialize())
        {    // Cannot run NDI. Most likely because the CPU is not sufficient (see SDK documentation).
            // you can check this directly with a call to NDIlib_is_supported_CPU()
            printf("Cannot run NDI.");
            return 0;
        }
        
        // Catch interrupt so that we can shut down gracefully
        signal(SIGINT, sigint_handler);
        
        // Create an NDI source that is called "My Video" and is clocked to the video.
        NDIlib_send_create_t NDI_send_create_desc;
        NDI_send_create_desc.p_ndi_name = "My Video";
        
        // We create the NDI sender
        NDIlib_send_instance_t pNDI_send = p_NDILib->NDIlib_send_create(&NDI_send_create_desc);
        if (!pNDI_send)
        {
            printf("Cannot create NDI send instance.");
            return 0;
        }
        
        // We are going to create a 1920x1080 interlaced frame at 29.97Hz.
        
        NDI_video_frame.FourCC = NDIlib_FourCC_type_BGRA;
        NDI_video_frame.xres = 1280;
        NDI_video_frame.yres = 720;
        NDI_video_frame.line_stride_in_bytes = bytesPerLine;
        NDI_video_frame.p_data = pData;
        
        
        // We will send 1000 frames of video.
        while (!exit_loop)
        {    // Get the current time
            const auto start_time = std::chrono::high_resolution_clock::now();
            
            // Send 200 frames
            for (int idx = 0; !exit_loop && idx < 200; idx++)
            {
                p_NDILib->NDIlib_send_send_video_v2(pNDI_send, &NDI_video_frame);
            }
            
            // Get the end time
            const auto end_time = std::chrono::high_resolution_clock::now();
            
            // Just display something helpful
            printf("200 frames sent, average fps=%1.2f\n", 200.0f / std::chrono::duration_cast<std::chrono::duration<float>>(end_time - start_time).count());
        }
        
        // Free the video frame
        free(NDI_video_frame.p_data);
        
        // Destroy the NDI sender
        p_NDILib->NDIlib_send_destroy(pNDI_send);
        
        // Not required, but nice
        p_NDILib->NDIlib_destroy();
        
    }
    
    return 0;
}
