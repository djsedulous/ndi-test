//
//  ndi_test_wrapper.m
//  Test1
//
//  Created by Viacheslav Matchenko on 12/16/18.
//  Copyright © 2018 Viacheslav Matchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "NDITestWrapper.h"
#import <NDITest/NDITest.h>

enum NDIColorChannel {
    red,
    green,
    blue,
    alpha
};

@implementation NDITestWrapper

- (void)ndiSenderStart {
    self.width = 1280;
    self.height = 720;
    
    self.pitch = self.width * sizeof(Pixel);
    self.buffer = (uint8_t*) malloc(self.pitch * self.height);
    
    [NSThread detachNewThreadSelector:@selector(ndiTestFunc) toTarget:self withObject:nil];
}

- (void)ndiTestFunc {
    ndiTestFunc(self.buffer, self.pitch);
}

- (uint8_t)getColorUInt8Value:(NSColor *)nsColor colorChannel:(NDIColorChannel)colorChannel {
    CGFloat value;
    
    switch (colorChannel) {
        case red:
            value = [nsColor redComponent];
            break;
        case green:
            value = [nsColor greenComponent];
            break;
        case blue:
            value = [nsColor blueComponent];
            break;
        case alpha:
            value = [nsColor alphaComponent];
            break;
    }
    
    return (uint8_t) (value * 255);
}

- (Pixel)getPixelColor:(NSColor *)nsColor {
    Pixel color = {
        .red = [self getColorUInt8Value:nsColor colorChannel:red],
        .green = [self getColorUInt8Value:nsColor colorChannel:green],
        .blue = [self getColorUInt8Value:nsColor colorChannel:blue],
        .alpha = [self getColorUInt8Value:nsColor colorChannel:alpha]
    };
    
    return color;
}

- (void)ndiUpdateBufferByColor:(NSColor *)nsColor {
    Pixel color = [self getPixelColor:nsColor];

    for (size_t y = 0; y < self.height; ++y) {
        Pixel *row = (Pixel *) (self.buffer + y * self.pitch);
        for (size_t x = 0; x < self.width; ++x) {
            row[x] = color;
        }
    }
}

- (void)ndiUpdateBufferByImage:(NSImage *)nsImage {
    nsImage = [self resizedImage:nsImage toPixelDimensions:NSSizeFromCGSize(CGSizeMake(1280, 720))];
    NSBitmapImageRep *nsImageRep = [NSBitmapImageRep imageRepWithData:[nsImage TIFFRepresentation]];
    
    NSSize imageSize = [nsImageRep size];
    int imgWidth = (int) imageSize.width;
    int imgHeight = (int) imageSize.height;

    NSColor* nsTransparentColor = NSColor.whiteColor;
    nsTransparentColor = [nsTransparentColor colorUsingColorSpace:NSColorSpace.sRGBColorSpace];
    nsTransparentColor = [nsTransparentColor colorWithAlphaComponent:0.0];

    Pixel pxTransparentColor = [self getPixelColor:nsTransparentColor];

    for (int y = 0; y < self.height; ++y) {
        Pixel *row = (Pixel *) (self.buffer + y * self.pitch);

        for (int x = 0; x < self.width; ++x) {
            NSColor* nsColor = nil;
            Pixel pxColor = pxTransparentColor;

            if (x <= imgWidth && y <= imgHeight) {
                nsColor = [nsImageRep colorAtX:x y:y];
                pxColor = [self getPixelColor:nsColor];
            } else {
                pxColor = pxTransparentColor;
            }

            row[x] = pxColor;
        }
    }
}

- (void)ndiUpdateBufferFromBytes:(uint8_t *)bytes {
    self.buffer = bytes;
    
    updateFrameBuffer(self.buffer);
}

- (NSImage *)resizedImage:(NSImage *)sourceImage toPixelDimensions:(NSSize)newSize{
    
    if (!sourceImage.isValid) return nil;
    
    NSBitmapImageRep *rep = [[NSBitmapImageRep alloc]
                             initWithBitmapDataPlanes:NULL
                             pixelsWide:newSize.width
                             pixelsHigh:newSize.height
                             bitsPerSample:8
                             samplesPerPixel:4
                             hasAlpha:YES
                             isPlanar:NO
                             colorSpaceName:NSCalibratedRGBColorSpace
                             bytesPerRow:0
                             bitsPerPixel:0];
    rep.size = newSize;
    
    [NSGraphicsContext saveGraphicsState];
    [NSGraphicsContext setCurrentContext:[NSGraphicsContext graphicsContextWithBitmapImageRep:rep]];
    [sourceImage drawInRect:NSMakeRect(0, 0, newSize.width, newSize.height) fromRect:NSZeroRect operation:NSCompositingOperationCopy fraction:1.0];
    [NSGraphicsContext restoreGraphicsState];
    
    NSImage *newImage = [[NSImage alloc] initWithSize:newSize];
    [newImage addRepresentation:rep];
    return newImage;
}

@end
