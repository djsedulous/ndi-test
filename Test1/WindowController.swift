//
//  WindowController.swift
//  Test1
//
//  Created by Viacheslav Matchenko on 12/15/18.
//  Copyright © 2018 Viacheslav Matchenko. All rights reserved.
//

import Cocoa
import Foundation

class WindowController: NSWindowController {
    
    override func windowDidLoad() {
        window?.minSize = CGSize(width: 512, height: 288)
        window?.setFrame(CGRect(x: 0, y: 0, width: 1280, height: 720), display: true)
        
        window?.aspectRatio = CGSize(width: window!.frame.size.width, height: window!.frame.size.height)
    }
    
}
