//
//  ndi_test_wrapper.h
//  Test1
//
//  Created by Viacheslav Matchenko on 12/16/18.
//  Copyright © 2018 Viacheslav Matchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t alpha;
} Pixel;

@interface NDITestWrapper : NSObject

@property (atomic) uint8_t* buffer;

@property (atomic) size_t width;
@property (atomic) size_t height;

@property (atomic) size_t pitch;

- (void)ndiSenderStart;

- (void)ndiUpdateBufferByColor:(NSColor *)nsColor;

- (void)ndiUpdateBufferByImage:(NSImage *)nsImage;

- (void)ndiUpdateBufferFromBytes:(uint8_t *)bytes;

@end
