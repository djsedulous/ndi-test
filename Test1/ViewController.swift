//
//  ViewController.swift
//  Test1
//
//  Created by Viacheslav Matchenko on 12/13/18.
//  Copyright © 2018 Viacheslav Matchenko. All rights reserved.
//

import Cocoa
import AVFoundation
import CoreGraphics
import AVKit

class ViewController: NSViewController {

    var ndiTestWrappler: NDITestWrapper = NDITestWrapper()
    var player: AVPlayer!
    var videoOutput: AVPlayerItemVideoOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = Bundle.main.path(forResource: "xci_24122018_merry_christmas_lower_template_12_sec", ofType: "mov")!
        
        let asset = AVAsset(url: URL(fileURLWithPath: path))
        
        let mutableCompostion: AVMutableComposition = AVMutableComposition()
        
        let videoCompositionTrack: AVMutableCompositionTrack = mutableCompostion.addMutableTrack(withMediaType: .video, preferredTrackID: CMPersistentTrackID())!
        
//        let audioCompositionTrack: AVMutableCompositionTrack = mutableCompostion.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        let instructions: NSMutableArray = NSMutableArray()
        var size = CGSize.zero
        var time = CMTime.zero
        
        let videoAssetTrack: AVAssetTrack = asset.tracks(withMediaType: .video).first!
//        let audioAssetTrack: AVAssetTrack? = asset.tracks(withMediaType: .audio).first
        
        do {
            try videoCompositionTrack.insertTimeRange(CMTimeRange(start: .zero, duration: asset.duration), of: videoAssetTrack, at: time)
            
//            if audioAssetTrack != nil {
//                try audioCompositionTrack.insertTimeRange(CMTimeRange(start: .zero, duration: audioAssetTrack!.timeRange.duration), of: audioAssetTrack!, at: time)
//            }
        } catch {
            print("Error: \(error)")
        }
        
        time = CMTimeAdd(time, asset.duration)
        
        if __CGSizeEqualToSize(size, .zero) {
            size = videoAssetTrack.naturalSize
        }
        
        let mutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30);
        mutableVideoComposition.renderSize = size
        
        let videoCompositionInstruction = AVMutableVideoCompositionInstruction()
        let layerCompositionInstructoin = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack)
        layerCompositionInstructoin.setOpacity(1.0, at: CMTime.zero)
        videoCompositionInstruction.timeRange = CMTimeRange(start: CMTime.zero, duration: time)
        videoCompositionInstruction.layerInstructions = [layerCompositionInstructoin]
        instructions.add(videoCompositionInstruction)
        
        mutableVideoComposition.instructions = instructions as! [AVVideoCompositionInstructionProtocol]
        
        let frame = CGRect(x: 0.0, y: 0.0, width: 1280.0, height: 720.0)
        
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        let textLayer = CATextLayer()
        
        parentLayer.opacity = 1.0
        videoLayer.opacity = 1.0
        textLayer.opacity = 1.0
        
        parentLayer.frame = frame
        videoLayer.frame = frame
        textLayer.frame = CGRect(x: 0, y: 0, width: 300, height: 50)
        
        textLayer.backgroundColor = CGColor.white
        textLayer.foregroundColor = CGColor.black
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.string = "TEXT"
        textLayer.position = NSPointToCGPoint(NSPoint(x: NSMidX(textLayer.bounds), y: NSMidY(textLayer.bounds)));
        textLayer.fontSize = 24.0
        textLayer.opacity = 0
        
        let fadeInAnimation = CABasicAnimation(keyPath: "opacity")
        fadeInAnimation.duration = 1.0
        fadeInAnimation.fromValue = NSNumber(value: 0)
        fadeInAnimation.toValue = NSNumber(value: 1)
        fadeInAnimation.isRemovedOnCompletion = false
        fadeInAnimation.beginTime = CFTimeInterval(0.0)
        fadeInAnimation.fillMode = CAMediaTimingFillMode.forwards

        textLayer.add(fadeInAnimation, forKey: "textOpacityIN")
        
        
        parentLayer.addSublayer(videoLayer)
        parentLayer.addSublayer(textLayer)
        
        
        mutableVideoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        
        let exporter: AVAssetExportSession! = AVAssetExportSession(asset: mutableCompostion, presetName: AVAssetExportPresetPassthrough)
        
        var tempFileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("xci_24122018_merry_christmas_lower_template_12_sec2.mov", isDirectory: false)
        
        tempFileUrl = URL(fileURLWithPath: tempFileUrl.path)
        
        do {
            try FileManager.default.removeItem(at: tempFileUrl)
        } catch {}
        
        exporter.canPerformMultiplePassesOverSourceMediaData = true
        exporter.outputURL = tempFileUrl
        exporter.videoComposition = mutableVideoComposition
        exporter.outputFileType = AVFileType.mov
        exporter.shouldOptimizeForNetworkUse = true;

        let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: time)
        exporter.timeRange = timeRange
        
        exporter.exportAsynchronously {
            print("\(String(describing: exporter.error))")
        }
        
        /*
        let playerItem: AVPlayerItem = AVPlayerItem(asset: mutableCompostion)
        playerItem.videoComposition = mutableVideoComposition
        
        let settings: [String : AnyObject] = [kCVPixelBufferPixelFormatTypeKey as String :  NSNumber.init(value: kCVPixelFormatType_32BGRA),
                                              kCVPixelBufferWidthKey as String: NSNumber(value: 1280),
                                              kCVPixelBufferHeightKey as String: NSNumber(value: 720)]
        self.videoOutput = AVPlayerItemVideoOutput.init(pixelBufferAttributes: settings)
        
        playerItem.add(self.videoOutput);
        
        self.player = AVPlayer(playerItem: playerItem)
        let playerLayer: AVPlayerLayer = AVPlayerLayer(player: self.player)
        
        playerLayer.frame = frame
    
        let timeScale = CMTimeScale(NSEC_PER_MSEC)
        let currentTime = CMTime(seconds: 0.001, preferredTimescale: timeScale)
        
        player.addPeriodicTimeObserver(forInterval: currentTime, queue: .main) { [weak self] time in
            let itemTime = self!.videoOutput.itemTime(forHostTime: CACurrentMediaTime())
            
            if self!.videoOutput.hasNewPixelBuffer(forItemTime: itemTime) {
                
                if let pixelBuffer = self!.videoOutput.copyPixelBuffer(forItemTime: itemTime, itemTimeForDisplay: nil) {
                    
                    CVPixelBufferLockBaseAddress(pixelBuffer, [.readOnly])
                    let baseAddress:UnsafeMutableRawPointer? = CVPixelBufferGetBaseAddress(pixelBuffer)
                    CVPixelBufferUnlockBaseAddress(pixelBuffer, [.readOnly])
                    
                    if ((baseAddress) != nil) {
                        let bytes = baseAddress!.assumingMemoryBound(to: UInt8.self)
                        
                        self!.ndiTestWrappler.ndiUpdateBuffer(fromBytes: bytes)
                    }
                }
            }
        }
        */
//        playerLayer.autoresizingMask = [.layerWidthSizable, .layerHeightSizable]
        self.view.layer = parentLayer
//        self.view.layer?.frame = frame
//        self.view.layer?.addSublayer(textLayer)
//        self.view.layer?.autoresizingMask = [.layerWidthSizable, .layerHeightSizable]
//        self.view.layer?.backgroundColor = CGColor.white
//        self.player.play()
    }

}

